package fr.lesptitsgourmets.lesptitsgourmets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.IngredientRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UnitDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Ingredient;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Unit;
import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.Price;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.IngredientServiceImpl;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class IngredientServiceTest {

	@InjectMocks
	IngredientServiceImpl ingredientService;

	@Mock
	private IngredientRepository ingredientRepository;

	@Before
	public void before() {

		ingredientRepository.deleteAll();
	}

	@Test
	public void testSaveIngredient() throws Exception {

		IngredientDto ingredientDto = new IngredientDto("Carotte", Price.low, 20, 
				new UnitDto("gramme"));

		Ingredient mockEntity = new Ingredient("Carotte", Price.low, 20, 
				new Unit("gramme"));

		when(ingredientRepository.save(any(Ingredient.class))).thenReturn(mockEntity);
		ResponseEntity res = ingredientService.createIngredient(ingredientDto);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(),ReturnContext.class);

		Assert.assertTrue(rc.getData() != null);
	}

	@Test
	public void testGetAllIngredient() throws Exception {
		
		Ingredient ingredient = new Ingredient("Rapé", Price.low, 1, 
				new Unit("gramme"));

		Ingredient ingredient2 = new Ingredient("Carotte", Price.low, 20, 
				new Unit("gramme"));

		List<Ingredient> listIngredient = new ArrayList<Ingredient>();
		listIngredient.add(ingredient);
		listIngredient.add(ingredient2);

		when(ingredientRepository.findAll()).thenReturn(listIngredient);

		ResponseEntity res = ingredientService.getAllIngredients();
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(),ReturnContext.class);

		Assert.assertTrue(rc.getData().size()== listIngredient.size());
	}
	
	@Test
	public void testDeleteIngredientById() throws Exception {

		ResponseEntity res = ingredientService.deleteIngredientById(2);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(),ReturnContext.class);
		Assert.assertTrue(rc.getStatus().equals(HttpStatus.OK));
	}
}
