package fr.lesptitsgourmets.lesptitsgourmets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import fr.lesptitsgourmets.lesptitsgourmets.controller.RecipeController;
import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.RecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.RecipeTransformers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("/applicationContent.xml")
@TestExecutionListeners(listeners = RecipeWebServiceTest.class, mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
public class RecipeWebServiceTest extends AbstractTestExecutionListener {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private static final String SERVICE_URI = "/recipe";

	private static Recipe getRecipeDataset;

	private static Recipe deleteRecipeDataset;

	@Autowired
	private RecipeController dao;

	@Override
	public void beforeTestClass(TestContext testContext) throws Exception {
		dao = testContext.getApplicationContext().getBean(RecipeController.class);

		getRecipeDataset = new Recipe("Chile con carne", 3, null, "Plat mexicain",
				new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme"));

		ReturnContext rc = new ObjectMapper()
				.readValue(dao.createRecipe("floflo", getRecipeDataset).getBody().toString(), ReturnContext.class);
		getRecipeDataset = RecipeTransformers.toRecipeEntity((RecipeDto) rc.getData());

		deleteRecipeDataset = new Recipe(1, "Carottes rapées", 8, "photo.png", "Elles sont bonnes mes carottes",
				new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme"));

		rc = new ObjectMapper().readValue(dao.createRecipe("floflo", getRecipeDataset).getBody().toString(),
				ReturnContext.class);
		deleteRecipeDataset = RecipeTransformers.toRecipeEntity((RecipeDto) rc.getData());
	}

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Override
	public void afterTestClass(TestContext testContext) throws Exception {
		dao.deleteOwnRecipe("floflo", getRecipeDataset.getId());
	}

	@Test
	@Rollback(true)
	@Commit
	@Transactional
	public void testSaveRecipe() throws Exception {
		String recipe = "{ \"name\": \"Chili con carne\",\r\n" + "        \"description\": \"Plat mexicain\",\r\n"
				+ "        \"creator\": {\r\n" + "            \"id\": 1,\r\n"
				+ "            \"email\": \"anonyme@anonyme.com\",\r\n" + "            \"password\": \"hashed\",\r\n"
				+ "            \"token\": null,\r\n" + "            \"username\": \"Anonyme\"\r\n" + "        }\r\n"
				+ "}";
		String jsonResponse = this.mockMvc
				.perform(post(SERVICE_URI).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON_UTF8).content(recipe))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		Recipe responseRecipe = JsonPath.parse(jsonResponse).read("$.data", Recipe.class);
		Assert.assertNotNull(responseRecipe.getId());
		Assert.assertFalse(responseRecipe.getId().toString().isEmpty());
	}

	@Test
	public void testGetNotFoundRecipe() throws Exception {
		String jsonResponse = this.mockMvc
				.perform(get(SERVICE_URI + "/" + "test").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString();
		ReturnContext response = JsonPath.parse(jsonResponse).read("$", ReturnContext.class);
		Assert.assertTrue(response.getStatus() == HttpStatus.NOT_FOUND);

	}

	@Test
	public void testGetRecipe() throws Exception {
		String jsonResponse = this.mockMvc
				.perform(get(SERVICE_URI + "/" + getRecipeDataset.getId()).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		Recipe responseRecipe = JsonPath.parse(jsonResponse).read("$.data", Recipe.class);
		Assert.assertNotNull(responseRecipe.getId());
		Assert.assertFalse(responseRecipe.getId().toString().isEmpty());
	}

	@Test
	public void testDeleteRecipe() throws Exception {
		Integer id = deleteRecipeDataset.getId();
		this.mockMvc.perform(delete(SERVICE_URI + "/" + id).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
		Assert.assertNull(dao.getRecipeById(null, id));
	}
}