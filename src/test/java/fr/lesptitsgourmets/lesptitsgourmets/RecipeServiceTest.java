package fr.lesptitsgourmets.lesptitsgourmets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.RecipeRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.RecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UserDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.RecipeServiceImpl;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class RecipeServiceTest {

	@InjectMocks
	RecipeServiceImpl recipeService;

	@Mock
	private RecipeRepository recipeRepository;

	@Before
	public void before() {

		recipeRepository.deleteAll();
	}

	@Test
	public void testSaveRecipe() throws Exception {

		Random rand = new Random();
		int id = rand.nextInt(Integer.MAX_VALUE) + 1;

		RecipeDto recipeDto = new RecipeDto(id, "Chile con carne", 3, null, "Plat mexicain", new UserDto("Anonyme"));

		Recipe mockEntity = new Recipe(id, "Chile con carne", 3, null, "Plat mexicain",
				new ApplicationUser("anonyme@anonyme.com", null, null, "Anonyme"));

		when(recipeRepository.save(any(Recipe.class))).thenReturn(mockEntity);
		ResponseEntity res = recipeService.createRecipe(recipeDto);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(), ReturnContext.class);

		Assert.assertTrue(rc.getData() != null);
	}

	@Test
	public void testGetRecipeById() throws Exception {

		Optional<Recipe> recipe = Optional
				.of(new Recipe(1, "Carottes rapées", 8, "photo.png", "Elles sont bonnes mes carottes",
						new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme")));

		when(recipeRepository.findById(1)).thenReturn(recipe);
		ResponseEntity res = recipeService.getRecipeById(1, null);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(), ReturnContext.class);
		Assert.assertTrue(rc.getData() != null);
	}

	@Test
	public void testDeleteRecipeById() throws Exception {

		ResponseEntity res = recipeService.deleteRecipeById(1);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(), ReturnContext.class);
		Assert.assertTrue(rc.getStatus().equals(HttpStatus.OK));
	}

	@Test
	public void testGetAllRecipe() throws Exception {

		Recipe recipe = new Recipe(1, "Carottes rapées", 8, "photo.png", "Elles sont bonnes mes carottes",
				new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme"));

		Recipe recipe2 = new Recipe(2, "Cake", 120, "photo.png", "Il est au citron et c'est bon",
				new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme"));

		Recipe recipe3 = new Recipe(3, "Saumon aux courgettes", -20, "photo.png", "Faut etre bizarre pour aimer ça",
				new ApplicationUser("anonyme@anonyme.com", "hashed", null, "Anonyme"));

		List<Recipe> listRecipe = new ArrayList<Recipe>();
		listRecipe.add(recipe);
		listRecipe.add(recipe2);
		listRecipe.add(recipe3);

		Page<Recipe> pageRecipe = new PageImpl<Recipe>(listRecipe);

		when(recipeRepository.findAll(PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id")))).thenReturn(pageRecipe);

		ResponseEntity res = recipeService.getAllRecipes("", null);
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(), ReturnContext.class);

		Assert.assertTrue(rc.getData().size() == pageRecipe.getContent().size());
	}
}