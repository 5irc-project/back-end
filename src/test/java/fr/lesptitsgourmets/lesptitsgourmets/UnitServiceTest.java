package fr.lesptitsgourmets.lesptitsgourmets;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.UnitRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Unit;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.UnitServiceImpl;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class UnitServiceTest {

	@InjectMocks
	UnitServiceImpl unitService;

	@Mock
	private UnitRepository unitRepository;

	@Before
	public void before() {

		unitRepository.deleteAll();
	}
	
	@Test
	public void testGetAllUnit() throws Exception {

		Unit unit = new Unit("Gramme");

		List<Unit> listUnit = new ArrayList<Unit>();
		listUnit.add(unit);


		when(unitRepository.findAll()).thenReturn(listUnit);

		ResponseEntity res = unitService.getAllUnits();
		ReturnContext rc = new ObjectMapper().readValue(res.getBody().toString(),ReturnContext.class);

		Assert.assertTrue(rc.getData().size() == listUnit.size());
	}
}