package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class IngredientTransformers {

	public static List<IngredientDto> toIngredientDtoList(List<Ingredient> ingredients) {
		List<IngredientDto> ingredientDtos = new ArrayList<>();

		for(Ingredient ingredient: ingredients) {
			ingredientDtos.add(toIngredientDto(ingredient));
		}

		return ingredientDtos;
	}

	/*** Converter ***/
	public static Ingredient toIngredientEntity(IngredientDto ingredientDto) {

		Ingredient ingredient = new Ingredient();

		if(ingredientDto.getId() != null) {
			ingredient.setId(ingredientDto.getId());
		}
		if(ingredientDto.getName() != null) {
			ingredient.setName(ingredientDto.getName());
		}
		if(ingredientDto.getPrice() != null) {
			ingredient.setPrice(ingredientDto.getPrice());
		}
		if(ingredientDto.getUnit() != null) {
			ingredient.setUnit(UnitTransformers.toUnitEntity(ingredientDto.getUnit()));
		}

		ingredient.setKcal(ingredientDto.getKcal());

		return ingredient;
	}

	public static IngredientDto toIngredientDto(Ingredient ingredient) {

		IngredientDto ingredientDto = new IngredientDto();

		ingredientDto.setId(ingredient.getId());
		ingredientDto.setKcal(ingredient.getKcal());
		ingredientDto.setName(ingredient.getName());
		ingredientDto.setPrice(ingredient.getPrice());
		ingredientDto.setUnit(UnitTransformers.toUnitDto(ingredient.getUnit()));

		return ingredientDto;
	}
}
