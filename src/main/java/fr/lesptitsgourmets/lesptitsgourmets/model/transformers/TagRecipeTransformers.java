package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.TagRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.TagRecipe;

public class TagRecipeTransformers {

	public static Set<TagRecipe> toTagRecipeEntityList(List<TagRecipeDto> tagRecipeDtoList) {
		Set<TagRecipe> tagRecipeList = new HashSet<>();

		for (TagRecipeDto tagRecipeDto : tagRecipeDtoList) {

			TagRecipe tagRecipe = new TagRecipe();
			tagRecipe.setTag(TagTransformers.toTagEntity(tagRecipeDto.getTag()));
			tagRecipeList.add(tagRecipe);
		}

		return tagRecipeList;
	}

	public static List<TagRecipeDto> toTagRecipeDtoList(Collection<TagRecipe> tagRecipeList)
	{
		List<TagRecipeDto> tagRecipeDtoList = new ArrayList<TagRecipeDto>();

		for (TagRecipe tagRecipe : tagRecipeList) {

			TagRecipeDto tagRecipeDto = new TagRecipeDto();
			tagRecipeDto.setTag(TagTransformers.toTagDto(tagRecipe.getTag()));
			tagRecipeDtoList.add(tagRecipeDto);
		}

		return tagRecipeDtoList;
	}
}
