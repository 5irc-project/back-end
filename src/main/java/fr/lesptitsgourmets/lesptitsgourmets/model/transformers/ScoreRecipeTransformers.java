
package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.ScoreRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ScoreRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.ScoreRecipeId;

public class ScoreRecipeTransformers {

	public static ScoreRecipeDto toScoreRecipeDtoClear(ScoreRecipe entity) {

		ScoreRecipeDto dto = new ScoreRecipeDto();
		dto.setValue(entity.getValue());
		dto.setRecipeId(entity.getRecipe().getId());
		dto.setUser(UserTransformers.toUserDto(entity.getApplicationUser()));
		dto.setId(new ScoreRecipeId(dto.getRecipeId(), dto.getUser().getId()));

		return dto;
	}

	public static ScoreRecipeDto toScoreRecipeDto(ScoreRecipe entity) {

		ScoreRecipeDto dto = new ScoreRecipeDto();

		dto.setValue(entity.getValue());
		dto.setRecipe(RecipeTransformers.toRecipeDto(entity.getRecipe(), entity.getApplicationUser().getId()));
		dto.setUser(UserTransformers.toUserDto(entity.getApplicationUser()));

		return dto;
	}

	public static ScoreRecipe toScoreRecipeEntity(ScoreRecipeDto dto) {

		ScoreRecipe scoreRecipe = new ScoreRecipe();
		scoreRecipe.setId(dto.getId());

		scoreRecipe.setValue(dto.getValue());

		Recipe recipe = new Recipe();
		recipe.setId(dto.getRecipeId());
		scoreRecipe.setRecipe(recipe);
		scoreRecipe.setApplicationUser(UserTransformers.toUserEntity(dto.getUser()));
		return scoreRecipe;
	}

	public static ScoreRecipeDto toScoreRecipeDtoRecipe(ScoreRecipe entity) {

		ScoreRecipeDto dto = new ScoreRecipeDto();
		dto.setValue(entity.getValue());

		return dto;
	}
}