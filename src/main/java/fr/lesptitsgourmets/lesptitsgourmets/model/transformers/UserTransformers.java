package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UserDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;

public class UserTransformers {

	public static ApplicationUser toUserEntity(UserDto userDto) {

		ApplicationUser entity = new ApplicationUser();

		entity.setId(userDto.getId());

		if (userDto.getUsername() != null) {
			entity.setUsername(userDto.getUsername());
		}
		return entity;
	}

	public static UserDto toUserDto(ApplicationUser entity) {

		UserDto dto = new UserDto();

		dto.setId(entity.getId());

		if (entity.getUsername() != null) {
			dto.setUsername(entity.getUsername());
		}
		return dto;
	}
}
