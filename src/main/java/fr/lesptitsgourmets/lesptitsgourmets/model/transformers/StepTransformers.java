package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.StepDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Step;

public class StepTransformers {
	public static StepDto toStepDto(Step entity) {

		StepDto dto = new StepDto();

		dto.setId(entity.getId());
		dto.setDuration(entity.getDuration());
		dto.setNoorder(entity.getNoorder());

		if (entity.getDescription() != null) {
			dto.setDescription(entity.getDescription());
		}
		if (entity.getType() != null) {
			dto.setType(entity.getType());
		}
		return dto;
	}

	public static Step toStepEntity(StepDto stepDto) {

		Step entity = new Step();

		entity.setId(stepDto.getId());
		entity.setDuration(stepDto.getDuration());
		entity.setNoorder(stepDto.getNoorder());

		if (stepDto.getDescription() != null) {
			entity.setDescription(stepDto.getDescription());
		}
		if (stepDto.getType() != null) {
			entity.setType(stepDto.getType());
		}
		return entity;
	}
}
