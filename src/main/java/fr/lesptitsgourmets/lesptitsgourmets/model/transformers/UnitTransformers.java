package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UnitDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Unit;

public class UnitTransformers {
	
	public static UnitDto toUnitDto(Unit entity) {

		UnitDto dto = new UnitDto();

		dto.setId(entity.getId());
		dto.setName(entity.getName());

		return dto;
	}
	
	public static Unit toUnitEntity(UnitDto dto) {

		Unit unit = new Unit();

		unit.setId(dto.getId());
		unit.setName(dto.getName());

		return unit;
	}
}
