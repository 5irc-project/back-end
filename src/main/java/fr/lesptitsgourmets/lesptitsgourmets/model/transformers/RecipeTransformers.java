package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.RecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.ScoreRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.StepDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.IngredientRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ScoreRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Step;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.TagRecipe;

import java.util.*;

public class RecipeTransformers {

	public static List<RecipeDto> toRecipeDtoListClear(List<Recipe> recipeList, Integer idUser) {

		List<RecipeDto> recipeDtoList = new ArrayList<RecipeDto>();

		for (Recipe recipe : recipeList) {
			RecipeDto recipeDto = toRecipeDto(recipe, idUser);
			recipeDto.setIngredientrecipe(null);
			recipeDto.setTagRecipes(null);
			recipeDto.setSteps(null);

			if (idUser != null && recipe.getScoreRecipes() != null) // Si authentifié on renvoie ses recettes likées
			{
				List<ScoreRecipeDto> scoreRecipeList = new ArrayList<ScoreRecipeDto>();
				for (ScoreRecipe scoreRecipe : recipe.getScoreRecipes()) {
					if (scoreRecipe.getApplicationUser().getId() == idUser) {
						scoreRecipeList.add(ScoreRecipeTransformers.toScoreRecipeDtoRecipe(scoreRecipe));
						recipeDto.setScoreRecipes(scoreRecipeList);
					}
				}
			}
			recipeDtoList.add(recipeDto);
		}
		return recipeDtoList;
	}

	public static RecipeDto toRecipeDto(Recipe entity, Integer idUser) {

		RecipeDto dto = new RecipeDto();

		dto.setId(entity.getId());

		if (entity.getDescription() != null) {
			dto.setDescription(entity.getDescription());
		}

		if (entity.getName() != null) {
			dto.setName(entity.getName());
		}

		if (entity.getPhotoPath() != null) {
			dto.setPhotoPath(entity.getPhotoPath());
		}

		if (entity.getScore() != null) {
			dto.setScore(entity.getScore());
		}

		if (entity.getCreator() != null) {
			dto.setCreator(UserTransformers.toUserDto(entity.getCreator()));
		}

		if (entity.getSteps() != null) {
			List<StepDto> stepDtoList = new ArrayList<StepDto>();
			for (Step stepDto : entity.getSteps()) {
				stepDtoList.add(StepTransformers.toStepDto(stepDto));
			}
			Collections.sort(stepDtoList, new Comparator<StepDto>() {
				@Override
				public int compare(StepDto stepDto, StepDto t1) {
					if (stepDto.getNoorder() > t1.getNoorder()) {
						return 1;
					} else {
						return -1;
					}
				}
			});
			dto.setSteps(stepDtoList);
		}

		if (entity.getIngredientrecipe() != null) {
			dto.setIngredientrecipe(
					IngredientRecipeTransformers.toIngredientRecipeDtoList(entity.getIngredientrecipe()));
		}

		if (entity.getTagRecipes() != null) {
			dto.setTagRecipes(TagRecipeTransformers.toTagRecipeDtoList(entity.getTagRecipes()));
		}

		if (idUser != null && entity.getScoreRecipes() != null) {
			List<ScoreRecipeDto> scoreRecipeList = new ArrayList<ScoreRecipeDto>();
			for (ScoreRecipe scoreRecipe : entity.getScoreRecipes()) {
				if (scoreRecipe.getApplicationUser().getId() == idUser) {
					scoreRecipeList.add(ScoreRecipeTransformers.toScoreRecipeDtoRecipe(scoreRecipe));
					dto.setScoreRecipes(scoreRecipeList);
				}
			}
		}

		return dto;
	}

	public static Recipe toRecipeEntity(RecipeDto recipeDto) {

		Recipe entity = new Recipe();

		if (recipeDto.getName() != null) {
			entity.setName(recipeDto.getName());
		}

		if (recipeDto.getDescription() != null) {
			entity.setDescription(recipeDto.getDescription());
		}

		if (recipeDto.getPhotoPath() != null) {
			entity.setPhotoPath(recipeDto.getPhotoPath());
		}

		if (recipeDto.getCreator() != null) {
			entity.setCreator(UserTransformers.toUserEntity(recipeDto.getCreator()));
		}

		if (recipeDto.getSteps() != null) {

			Set<Step> stepList = new HashSet<Step>();
			for (StepDto stepDto : recipeDto.getSteps()) {
				stepList.add(StepTransformers.toStepEntity(stepDto));
			}

			for (Step step : stepList) {
				entity.addStep(step);
			}
			entity.setSteps(stepList);
		}
		if (recipeDto.getIngredientrecipe() != null) {

			Set<IngredientRecipe> list = IngredientRecipeTransformers
					.toIngredientRecipeEntityList(recipeDto.getIngredientrecipe());

			entity.setIngredientrecipe(list);
		}

		if (recipeDto.getTagRecipes() != null) {

			Set<TagRecipe> tagRecipeDtoList = new HashSet<TagRecipe>();
			tagRecipeDtoList.addAll(TagRecipeTransformers.toTagRecipeEntityList(recipeDto.getTagRecipes()));

			for (TagRecipe tagRecipe : tagRecipeDtoList) {
				entity.addTagRecipes(tagRecipe);
			}
			entity.setTagRecipes(tagRecipeDtoList);
		}

		return entity;
	}

}
