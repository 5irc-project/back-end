package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.TagDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagTransformers {

	public static TagDto toTagDto(Tag tag) {

		TagDto tagDto = new TagDto();
		tagDto.setId(tag.getId());
		tagDto.setName(tag.getName());
		return tagDto;
	}

	public static Tag toTagEntity(TagDto tagDto) {
		Tag tag = new Tag();
		tag.setId(tagDto.getId());
		tag.setName(tagDto.getName());
		return tag;
	}

	public static List<TagDto> toTagDtoList(List<Tag> tags) {
		List<TagDto> tagDtos = new ArrayList<>();

		for(Tag tag: tags) {
			tagDtos.add(toTagDto(tag));
		}

		return tagDtos;
	}
}
