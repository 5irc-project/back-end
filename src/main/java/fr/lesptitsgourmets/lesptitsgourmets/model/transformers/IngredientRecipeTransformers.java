package fr.lesptitsgourmets.lesptitsgourmets.model.transformers;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.IngredientRecipe;

import java.util.*;

public class IngredientRecipeTransformers {

	public static Set<IngredientRecipe> toIngredientRecipeEntityList(List<IngredientRecipeDto> ingredientRecipeDtoList)
	{
		Set<IngredientRecipe> ingredientRecipeList = new HashSet<>();

		for (IngredientRecipeDto ingredientRecipeDto : ingredientRecipeDtoList) {

			IngredientRecipe ingredientRecipe = new IngredientRecipe();
			ingredientRecipe.setIngredient(IngredientTransformers.toIngredientEntity(ingredientRecipeDto.getIngredient()));
			ingredientRecipe.setQuantity(ingredientRecipeDto.getQuantity());
			ingredientRecipeList.add(ingredientRecipe);
		}

		return ingredientRecipeList;
	}
	
	public static List<IngredientRecipeDto> toIngredientRecipeDtoList(Collection<IngredientRecipe> ingredientRecipeList)
	{
		List<IngredientRecipeDto> ingredientRecipeDtoList = new ArrayList<IngredientRecipeDto>();

		for (IngredientRecipe ingredientRecipe : ingredientRecipeList) {

			IngredientRecipeDto ingredientRecipeDto = new IngredientRecipeDto();
			ingredientRecipeDto.setIngredient(IngredientTransformers.toIngredientDto(ingredientRecipe.getIngredient()));
			ingredientRecipeDto.setQuantity(ingredientRecipe.getQuantity());
			ingredientRecipeDtoList.add(ingredientRecipeDto);
		}

		return ingredientRecipeDtoList;
	}
}
