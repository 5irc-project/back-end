package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.FavoriteRecipeId;

import javax.persistence.*;

@Entity
@Table(name = "favorite_recipe")
public class FavoriteRecipe {

	@EmbeddedId
	private FavoriteRecipeId id;

	@ManyToOne
	@JoinColumn(name = "recipe_id")
	@MapsId("recipe_id")
	private Recipe recipe;

	@ManyToOne
	@JoinColumn(name = "user_account_id")
	@MapsId("user_account_id")
	private ApplicationUser applicationUser;

	private FavoriteRecipe() {
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public FavoriteRecipeId getId() {
		return id;
	}

	public void setId(FavoriteRecipeId id) {
		this.id = id;
	}
}
