package fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TagRecipeId implements Serializable {

    private static final long serialVersionUID = -8887544357276003407L;

    @Column(name = "recipe_id", nullable = false)
    private Integer recipeId;

    @Column(name = "tag_id", nullable = false)
    private Integer tagId;

    public TagRecipeId() {
    }

    public TagRecipeId(Integer recipeId, Integer tagId) {
    	this.recipeId = recipeId;
    	this.tagId = tagId;
    }
    
    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagRecipeId that = (TagRecipeId) o;
        return recipeId.equals(that.recipeId) &&
                tagId.equals(that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeId, tagId);
    }
}