package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.ScoreRecipeId;

import javax.persistence.*;

@Entity
@Table(name = "score_recipe")
public class ScoreRecipe {

	@EmbeddedId
	private ScoreRecipeId id;

	@ManyToOne
	@JoinColumn(name = "recipe_id")
	@MapsId("recipe_id")
	private Recipe recipe;

	@ManyToOne
	@JoinColumn(name = "user_account_id")
	@MapsId("user_account_id")
	private ApplicationUser applicationUser;

	@Column(name = "value", nullable = false)
	private int value;

	public ScoreRecipe() {
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public ScoreRecipeId getId() {
		return id;
	}

	public void setId(ScoreRecipeId id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
