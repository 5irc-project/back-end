package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.StepType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "step")
public class Step {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "noorder", nullable = false)
	private int noorder;

	@Column(name = "duration", nullable = false)
	private int duration;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private StepType type;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "recipe_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Recipe recipe;

	public Step() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNoorder() {
		return noorder;
	}

	public void setNoorder(int noorder) {
		this.noorder = noorder;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StepType getType() {
		return type;
	}

	public void setType(StepType type) {
		this.type = type;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
}
