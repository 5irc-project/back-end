package fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class IngredientRecipeId implements Serializable {

    private static final long serialVersionUID = -4809503511626437223L;

    @Column(name = "recipe_id")
    private Integer recipeId;

    @Column(name = "ingredient_id")
    private Integer ingredientId;

    public IngredientRecipeId() {
    }

    public IngredientRecipeId(Integer recipeId, Integer ingredientId) {
        super();
        this.recipeId = recipeId;
        this.ingredientId = ingredientId;
    }

    public Integer getRecipeId() {
        return recipeId;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof IngredientRecipeId))
            return false;
        IngredientRecipeId that = (IngredientRecipeId) o;
        return Objects.equals(getRecipeId(), that.getRecipeId())
                && Objects.equals(getIngredientId(), that.getIngredientId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRecipeId(), getIngredientId());
    }
}
