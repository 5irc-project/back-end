package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.IngredientRecipeId;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ingredient_recipe")
public class IngredientRecipe{

	@EmbeddedId
	private IngredientRecipeId id;

	@Column(name = "quantity", nullable = false)
	private BigDecimal quantity;

	@ManyToOne
	@JoinColumn(name = "recipe_id")
	@MapsId("recipetId")
	private Recipe recipe;

	@ManyToOne
	@JoinColumn(name = "ingredient_id")
	@MapsId("ingredientId")
	private Ingredient ingredient;

	public IngredientRecipe() {
	}

	public IngredientRecipe(BigDecimal quantity, Recipe recipe, Ingredient ingredient) {
		this.quantity = quantity;
		this.recipe = recipe;
		this.ingredient = ingredient;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	public IngredientRecipeId getId() {
		return id;
	}

	public void setId(IngredientRecipeId id) {
		this.id = id;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
