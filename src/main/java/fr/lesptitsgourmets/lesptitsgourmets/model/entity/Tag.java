package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tag")
public class Tag {
	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "name", nullable = false, length = 25)
	private String name;

	@OneToMany(mappedBy = "tag")
	private Set<TagRecipe> tagRecipes;

	public Tag() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TagRecipe> getTagRecipes() {
		return tagRecipes;
	}

	public void setTagRecipes(Set<TagRecipe> tagRecipes) {
		this.tagRecipes = tagRecipes;
	}
}
