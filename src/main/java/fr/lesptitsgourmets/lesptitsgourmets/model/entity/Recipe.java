package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recipe")
public class Recipe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "name", length = 40, nullable = false)
	private String name;

	@Column(name = "score", nullable = false, columnDefinition = "integer default '0'")
	private Integer score = 0;

	@Column(name = "photo_path")
	private String photoPath;

	@Column(name = "description", length = 255)
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
			name = "user_account_id",
			nullable = false)
	private ApplicationUser creator;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "recipe")
	private Set<Step> steps = new HashSet<>();

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "recipe")
	private Set<IngredientRecipe> ingredientrecipe;

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "recipe")
	private Set<TagRecipe> tagRecipes = new HashSet<>();

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "recipe")
	private Set<ScoreRecipe> scoreRecipes;

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "recipe")
	private Set<FavoriteRecipe> favoriteRecipes;

	public Recipe() {
	}

	public Recipe(String name, String photoPath, String description, ApplicationUser creator) {
		super();
		this.name = name;
		this.photoPath = photoPath;
		this.description = description;
		this.creator = creator;
	}

	public Recipe(String name, int score, String photoPath, String description, ApplicationUser creator) {
		super();
		this.name = name;
		this.score = score;
		this.photoPath = photoPath;
		this.description = description;
		this.creator = creator;
	}

	public Recipe(Integer id, String name, int score, String photoPath, String description, ApplicationUser creator) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
		this.photoPath = photoPath;
		this.description = description;
		this.creator = creator;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ApplicationUser getCreator() {
		return creator;
	}

	public void setCreator(ApplicationUser creator) {
		this.creator = creator;
	}

	public Set<Step> getSteps() {
		return steps;
	}

	public void setSteps(Set<Step> steps) {
		this.steps = steps;
	}

	public Set<TagRecipe> getTagRecipes() {
		return tagRecipes;
	}

	public void setTagRecipes(Set<TagRecipe> tagRecipes) {
		this.tagRecipes = tagRecipes;
	}

	public Set<ScoreRecipe> getScoreRecipes() {
		return scoreRecipes;
	}

	public void setScoreRecipes(Set<ScoreRecipe> scoreRecipes) {
		this.scoreRecipes = scoreRecipes;
	}

	public Set<FavoriteRecipe> getFavoriteRecipes() {
		return favoriteRecipes;
	}

	public void setFavoriteRecipes(Set<FavoriteRecipe> favoriteRecipes) {
		this.favoriteRecipes = favoriteRecipes;
	}

	public Set<IngredientRecipe> getIngredientrecipe() {
		return ingredientrecipe;
	}

	public void setIngredientrecipe(Set<IngredientRecipe> ingredientrecipe) {
		this.ingredientrecipe = ingredientrecipe;
	}

	public void addStep(Step step) {
		steps.add(step);
		step.setRecipe(this);
	}

	public void removeStep(Step step) {
		step.setRecipe(null);
		steps.remove(step);
	}

	public void addTagRecipes(TagRecipe tagRecipe) {
		tagRecipes.add(tagRecipe);
		tagRecipe.setRecipe(this);
	}

	public void removeTagRecipes(TagRecipe tagRecipe) {
		tagRecipe.setRecipe(null);
		tagRecipes.remove(tagRecipe);
	}
}
