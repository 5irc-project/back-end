package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user_account")
public class ApplicationUser implements UserDetails {

	private static final long serialVersionUID = -7120057495442540444L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "email", nullable = false, length = 255, unique = true)
	private String email;

	@Column(name = "password", nullable = false, length = 64)
	private String password;

	@JsonIgnore
	@Column(name = "token", length = 64)
	private String token;

	@Column(name = "username", nullable = false, length = 25)
	private String username;

	@Transient
	private boolean enabled;
	@Transient
	private boolean accountNonExpired;
	@Transient
	private boolean credentialsNonExpired;
	@Transient
	private boolean accountNonLocked;
	@Transient
	private Set<GrantedAuthority> authorities;

	@OneToMany(cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "creator")
	private Set<Recipe> recipes = new HashSet<>();

	@OneToMany(mappedBy = "applicationUser")
	private Set<ScoreRecipe> scoreRecipes;

	@OneToMany(mappedBy = "applicationUser")
	private Set<FavoriteRecipe> favoriteRecipes;

	public ApplicationUser() {
		this.authorities = new HashSet<>();
		this.enabled = true;
		this.accountNonExpired = true;
		this.credentialsNonExpired = true;
		this.accountNonLocked = true;
		this.addAuthority(new SimpleGrantedAuthority("USER"));
	}

	public ApplicationUser(String email, Object u, Object o, String username) {
		this.email = email;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getUsername() {
		return this.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<ScoreRecipe> getScoreRecipes() {
		return scoreRecipes;
	}

	public void setScoreRecipes(Set<ScoreRecipe> scoreRecipes) {
		this.scoreRecipes = scoreRecipes;
	}

	public Set<FavoriteRecipe> getFavoriteRecipes() {
		return favoriteRecipes;
	}

	public void setFavoriteRecipes(Set<FavoriteRecipe> favoriteRecipes) {
		this.favoriteRecipes = favoriteRecipes;
	}

	public Set<Recipe> getRecipes() {
		return recipes;
	}

	public void setRecipes(Set<Recipe> recipes) {
		this.recipes = recipes;
	}

	public void addAuthority(SimpleGrantedAuthority authority) {
		this.authorities.add(authority);
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
}
