package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.Price;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ingredient")
public class Ingredient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "name", nullable = false, length = 25)
	private String name;

	@Column(name = "price")
	@Enumerated(EnumType.STRING)
	private Price price;

	@Column(name = "kcal")
	private int kcal;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "unit_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
	private Unit unit;

	@OneToMany(mappedBy = "ingredient")
	private Set<IngredientRecipe> ingredientRecipes;

	public Ingredient() {
	}

	public Ingredient(String name, Price price, int kcal, Unit unit) {
		super();
		this.name = name;
		this.price = price;
		this.kcal = kcal;
		this.unit = unit;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public int getKcal() {
		return kcal;
	}

	public void setKcal(int kcal) {
		this.kcal = kcal;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

    public Set<IngredientRecipe> getIngredientRecipes() {
        return ingredientRecipes;
    }

    public void setIngredientRecipes(Set<IngredientRecipe> ingredientRecipes) {
        this.ingredientRecipes = ingredientRecipes;
    }
}
