package fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ScoreRecipeId implements Serializable {

	private static final long serialVersionUID = -8887544357276003407L;

	@Column(name = "recipe_id", nullable = false)
	private Integer recipeId;

	@Column(name = "user_account_id", nullable = false)
	private Integer userAccountId;

	public ScoreRecipeId() {
	}

	public ScoreRecipeId(Integer recipeId, Integer userAccountId) {
		this.recipeId = recipeId;
		this.userAccountId = userAccountId;
	}

	public Integer getRecipeId() {
		return recipeId;
	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		ScoreRecipeId that = (ScoreRecipeId) o;
		return Objects.equals(recipeId, that.recipeId) && Objects.equals(userAccountId, that.userAccountId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(recipeId, userAccountId);
	}
}
