package fr.lesptitsgourmets.lesptitsgourmets.model.entity;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.TagRecipeId;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tag_recipe")
public class TagRecipe {

    @EmbeddedId
    private TagRecipeId id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @MapsId("recipe_id")
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "tag_id")
    @MapsId("tag_id")
    private Tag tag;

    public TagRecipeId getId() {
        return id;
    }

    public void setId(TagRecipeId id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
