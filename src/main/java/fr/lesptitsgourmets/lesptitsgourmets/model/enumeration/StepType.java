package fr.lesptitsgourmets.lesptitsgourmets.model.enumeration;

public enum StepType {
	
	cooking,
	waiting,
	preparation;

}
