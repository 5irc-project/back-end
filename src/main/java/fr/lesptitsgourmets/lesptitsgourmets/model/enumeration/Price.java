package fr.lesptitsgourmets.lesptitsgourmets.model.enumeration;

public enum Price {
	low, middle, high;
}
