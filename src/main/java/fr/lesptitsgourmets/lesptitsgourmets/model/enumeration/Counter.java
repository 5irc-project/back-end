package fr.lesptitsgourmets.lesptitsgourmets.model.enumeration;

public enum Counter {

	MINUSONE(-1),
	PLUSONE(1);
	
	private int name;

	Counter(int value) 
	{
		this.name = value;
	}

	public String toString() 
	{
		return this.toString();
	}
	
	public int getName()
	{
		return this.name;
	}
}
