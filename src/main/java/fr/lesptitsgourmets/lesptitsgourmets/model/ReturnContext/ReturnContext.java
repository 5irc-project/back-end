package fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ReturnContext {

	private HttpStatus status;

	private List<?> data;

	private String message;

	public ReturnContext(HttpStatus status, List<?> objectList, String message) {
		this.status = status;
		this.message = message;
		this.data = objectList;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> dataList) {
		this.data = dataList;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseEntity<Object> buildResponseEntity() {
		return new ResponseEntity<>(this, this.getStatus());
	}
}
