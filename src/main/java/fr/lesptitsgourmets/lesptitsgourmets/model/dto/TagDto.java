package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class TagDto {

	private Integer id;
	@JsonInclude(Include.NON_EMPTY)
	private String name;

	public TagDto() {
	}

    public Integer getId() {
        return id;
    }

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
