package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.math.BigDecimal;

public class IngredientRecipeDto {

	@JsonInclude(Include.NON_EMPTY)
	private RecipeDto recipe;
	private IngredientDto ingredient;
	private BigDecimal quantity;

	public IngredientRecipeDto() {
	}

	public RecipeDto getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDto recipe) {
		this.recipe = recipe;
	}

	public IngredientDto getIngredient() {
		return ingredient;
	}

	public void setIngredient(IngredientDto ingredient) {
		this.ingredient = ingredient;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
