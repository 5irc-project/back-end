package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.ScoreRecipeId;

public class ScoreRecipeDto {

	@JsonInclude(Include.NON_EMPTY)
	private ScoreRecipeId id;
	@JsonInclude(Include.NON_EMPTY)
	private RecipeDto recipe;
	@JsonInclude(Include.NON_EMPTY)
	private Integer recipeId;
	@JsonInclude(Include.NON_EMPTY)
	private UserDto user;
	private int value;

	public ScoreRecipeDto() {
	}

	public ScoreRecipeDto(ScoreRecipeId id, RecipeDto recipe, UserDto user, int value) {
		super();
		this.recipe = recipe;
		this.user = user;
		this.value = value;
	}

	public RecipeDto getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDto recipe) {
		this.recipe = recipe;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public Integer getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}

	public ScoreRecipeId getId() {
		return id;
	}

	public void setId(ScoreRecipeId id) {
		this.id = id;
	}
}
