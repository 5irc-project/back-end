package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

public class RecipeDto {

	private Integer id;
	private String name;
	private Integer score;
	private String photoPath;
	private String description;
	private UserDto creator;
	@JsonInclude(Include.NON_EMPTY)
	private List<StepDto> steps;
	@JsonInclude(Include.NON_EMPTY)
	private List<IngredientRecipeDto> ingredientrecipe;
	@JsonInclude(Include.NON_EMPTY)
	private List<TagRecipeDto> tagRecipes;
	@JsonInclude(Include.NON_EMPTY)
	private List<ScoreRecipeDto> scoreRecipes;

	public RecipeDto() {
	}

	public RecipeDto(Integer id, String name, Integer score, String photoPath, String description, UserDto creator) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
		this.photoPath = photoPath;
		this.description = description;
		this.creator = creator;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserDto getCreator() {
		return creator;
	}

	public void setCreator(UserDto creator) {
		this.creator = creator;
	}

	public List<StepDto> getSteps() {
		return steps;
	}

	public void setSteps(List<StepDto> steps) {
		this.steps = steps;
	}

	public List<IngredientRecipeDto> getIngredientrecipe() {
		return ingredientrecipe;
	}

	public void setIngredientrecipe(List<IngredientRecipeDto> ingredientrecipe) {
		this.ingredientrecipe = ingredientrecipe;
	}

	public List<TagRecipeDto> getTagRecipes() {
		return tagRecipes;
	}

	public void setTagRecipes(List<TagRecipeDto> tagRecipes) {
		this.tagRecipes = tagRecipes;
	}

	public List<ScoreRecipeDto> getScoreRecipes() {
		return scoreRecipes;
	}

	public void setScoreRecipes(List<ScoreRecipeDto> scoreRecipes) {
		this.scoreRecipes = scoreRecipes;
	}
}
