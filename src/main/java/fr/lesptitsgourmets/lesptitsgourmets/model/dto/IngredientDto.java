package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.Price;


public class IngredientDto {

	private Integer id;
	private String name;
	private Price price;
	private int kcal;
	private UnitDto unit;

	public IngredientDto() {
	}

	public IngredientDto(String name, Price price, int kcal, UnitDto unit) {
		super();
		this.name = name;
		this.price = price;
		this.kcal = kcal;
		this.unit = unit;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public int getKcal() {
		return kcal;
	}

	public void setKcal(int kcal) {
		this.kcal = kcal;
	}

	public UnitDto getUnit() {
		return unit;
	}

	public void setUnit(UnitDto unit) {
		this.unit = unit;
	}
}
