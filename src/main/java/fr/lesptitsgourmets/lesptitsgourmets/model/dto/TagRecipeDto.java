package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class TagRecipeDto {

	private TagDto tag;
	
	@JsonInclude(Include.NON_EMPTY)
	private RecipeDto recipe;

	public TagRecipeDto(){}
	
	public TagDto getTag() {
		return tag;
	}

	public void setTag(TagDto tag) {
		this.tag = tag;
	}

	public RecipeDto getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDto recipe) {
		this.recipe = recipe;
	}	
}
