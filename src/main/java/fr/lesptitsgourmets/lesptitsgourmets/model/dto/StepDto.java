package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.StepType;

public class StepDto {

	private Integer id;
	private int noorder;
	private int duration;
	private String description;
	private StepType type;

	public StepDto() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNoorder() {
		return noorder;
	}

	public void setNoorder(int noorder) {
		this.noorder = noorder;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StepType getType() {
		return type;
	}

	public void setType(StepType type) {
		this.type = type;
	}
}
