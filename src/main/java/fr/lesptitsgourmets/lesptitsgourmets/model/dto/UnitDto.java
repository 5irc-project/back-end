package fr.lesptitsgourmets.lesptitsgourmets.model.dto;

public class UnitDto {

	private Integer id;
	private String name;

	public UnitDto() {
	}

	public UnitDto(String name) {
		super();
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
