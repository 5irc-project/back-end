package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.IngredientRecipe;

public interface IngredientRecipeRepository extends JpaRepository<IngredientRecipe, Integer> {

}
