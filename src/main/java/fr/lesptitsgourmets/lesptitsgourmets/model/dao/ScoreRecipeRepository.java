package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ScoreRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.ScoreRecipeId;

public interface ScoreRecipeRepository extends JpaRepository<ScoreRecipe, ScoreRecipeId> {
}
