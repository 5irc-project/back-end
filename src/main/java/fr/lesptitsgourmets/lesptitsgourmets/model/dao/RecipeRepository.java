package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;

public interface RecipeRepository extends JpaRepository<Recipe, Integer> {
 
	Page<Recipe> findByTagRecipes_Tag_Id(Integer id, Pageable pageable);

    Page<Recipe> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
    
    @Query("SELECT r FROM Recipe r where r.id= :idRecipe and r.creator.id = :idCreator") 
    Optional<Recipe> findByIdAndCreatorId(@Param("idRecipe") int idRecipe, @Param("idCreator") int idCreator);
}
