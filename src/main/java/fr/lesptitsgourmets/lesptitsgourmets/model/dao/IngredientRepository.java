package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Ingredient;

public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {
}
