package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {

}
