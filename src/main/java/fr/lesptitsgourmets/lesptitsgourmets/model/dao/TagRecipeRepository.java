package fr.lesptitsgourmets.lesptitsgourmets.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.lesptitsgourmets.lesptitsgourmets.model.entity.TagRecipe;

public interface TagRecipeRepository extends JpaRepository<TagRecipe, Integer> {

}
