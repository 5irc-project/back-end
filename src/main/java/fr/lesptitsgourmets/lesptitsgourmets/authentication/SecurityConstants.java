package fr.lesptitsgourmets.lesptitsgourmets.authentication;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String REGISTER_URL = "/users/register";
    public static final String SIGN_IN_URL = "/login";
    public static final String INGREDIENT_URL ="/ingredient";
    public static final String RECIPE_URL ="/recipe";
}
