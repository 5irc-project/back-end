package fr.lesptitsgourmets.lesptitsgourmets.authentication;

import com.auth0.jwt.JWT;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants.EXPIRATION_TIME;
import static fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants.TOKEN_PREFIX;
import static fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants.SECRET;

public final class TokenUtils {

    public static String getEmailFromToken(String token) {
        return JWT.require(HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }

    public static ArrayList<GrantedAuthority> getRolesFromToken(String token) {
        String [] tabRoles = JWT.require(HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getClaim("roles").asArray(String.class);
        ArrayList<GrantedAuthority> roles = new ArrayList<>();
        if (tabRoles != null) {
            for (String role : tabRoles) {
                roles.add(new SimpleGrantedAuthority(role));
            }
        }

        return roles;
    }

    public static String generateToken(String email, Collection<GrantedAuthority> authorities) {
        String[] roles = new String[authorities.size()];
        int i = 0;
        for (GrantedAuthority role : authorities) {
            roles[i] = role.toString();
            i++;
        }
        return JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withArrayClaim("roles", roles)
                .sign(HMAC512(SECRET.getBytes()));
    }
}