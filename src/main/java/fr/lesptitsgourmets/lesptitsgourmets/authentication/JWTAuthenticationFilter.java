package fr.lesptitsgourmets.lesptitsgourmets.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.UserServiceImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants.HEADER_STRING;
import static fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private UserServiceImpl userService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserServiceImpl userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            ApplicationUser creds = new ObjectMapper()
                    .readValue(req.getInputStream(), ApplicationUser.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            creds.getAuthorities())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        String token = TokenUtils.generateToken(((User) auth.getPrincipal()).getUsername(), ((User) auth.getPrincipal()).getAuthorities());
        try {
            ApplicationUser user = userService.getUserByEmail(((User) auth.getPrincipal()).getUsername());
            user.setToken(token);
            userService.updateUser(user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        res.getWriter().write("{}");
        res.getWriter().flush();
        res.getWriter().close();
    }
}