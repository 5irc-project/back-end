package fr.lesptitsgourmets.lesptitsgourmets.controller;

import fr.lesptitsgourmets.lesptitsgourmets.service.impl.UnitServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UnitController {

    @Autowired
    private UnitServiceImpl unitService;

    @GetMapping(value = "/units")
    public ResponseEntity getAllUnits() {

        return this.unitService.getAllUnits();
    }
}
