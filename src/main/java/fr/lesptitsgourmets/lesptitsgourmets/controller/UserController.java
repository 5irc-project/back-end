package fr.lesptitsgourmets.lesptitsgourmets.controller;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.UserRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping(value = "/register")
    public ResponseEntity signUp(@RequestBody ApplicationUser account) {
        Optional<ApplicationUser> existingAccount = userRepository.findByEmail(account.getEmail());
        if (!existingAccount.isPresent()) {
            if ((account.getPassword() == null) || (account.getPassword().isEmpty())) {
                return new ReturnContext(HttpStatus.BAD_REQUEST,
                        null,
                        "Password should not be empty").buildResponseEntity();
            }
            account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
            List<ApplicationUser> returnUsers = new ArrayList<>();
            returnUsers.add(userRepository.save(account));
            return new ReturnContext(HttpStatus.CREATED,
                    returnUsers,
                    "Account created").buildResponseEntity();
        }
        return new ReturnContext(HttpStatus.NOT_ACCEPTABLE,
                null,
                "User already exist").buildResponseEntity();
    }
}
