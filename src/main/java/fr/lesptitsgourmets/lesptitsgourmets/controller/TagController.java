package fr.lesptitsgourmets.lesptitsgourmets.controller;

import fr.lesptitsgourmets.lesptitsgourmets.service.impl.TagServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class TagController {

    @Autowired
    private TagServiceImpl tagService;

    @GetMapping(value = "/tags")
    public ResponseEntity getAllTags() {

        return this.tagService.getAllTags();
    }
}
