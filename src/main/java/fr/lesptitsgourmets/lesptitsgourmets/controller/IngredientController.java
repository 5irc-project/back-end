package fr.lesptitsgourmets.lesptitsgourmets.controller;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientDto;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.IngredientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class IngredientController {

    @Autowired
    private IngredientServiceImpl ingredientService;

	@Secured("ROLE_USER")
	@PostMapping(value = "/ingredient")
	public ResponseEntity createIngredient(@RequestBody IngredientDto ingredientDto) {
		return this.ingredientService.createIngredient(ingredientDto);
	}

    @GetMapping(value = "/ingredients")
    public ResponseEntity getAllIngredients() {

        return this.ingredientService.getAllIngredients();
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping(value = "/ingredient/{id}")
    public ResponseEntity deleteIngredientById(@PathVariable(value = "id") int id) {

        return this.ingredientService.deleteIngredientById(id);
    }
}
