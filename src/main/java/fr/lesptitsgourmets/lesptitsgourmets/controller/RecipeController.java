
package fr.lesptitsgourmets.lesptitsgourmets.controller;

import fr.lesptitsgourmets.lesptitsgourmets.authentication.SecurityConstants;
import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.ScoreRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ScoreRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.RecipeTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.ScoreRecipeTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.UserTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.RecipeServiceImpl;
import fr.lesptitsgourmets.lesptitsgourmets.service.impl.UserServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import twitter4j.TwitterException;

@RestController
@CrossOrigin
public class RecipeController {

	@Autowired
	private RecipeServiceImpl recipeService;

	@Autowired
	private UserServiceImpl userService;

	@GetMapping(value = "/recipes")
	public ResponseEntity getAllRecipes(
			@RequestHeader(required = false, name = SecurityConstants.HEADER_STRING) String authorization,
			@RequestParam(value = "name", required = false, defaultValue = "") String name) {

		if (authorization == null) {
			return this.recipeService.getAllRecipes(name, null);
		} else {
			ApplicationUser applicationUser = userService.getUserByToken(authorization);

			if (applicationUser != null) {
				return this.recipeService.getAllRecipes(name, applicationUser.getId());
			} else {
				return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
						.buildResponseEntity();
			}
		}
	}

	@GetMapping(value = "/recipe/{id}")
	public ResponseEntity getRecipeById(
			@RequestHeader(required = false, name = SecurityConstants.HEADER_STRING) String authorization,
			@PathVariable(value = "id") int id) {

		if (authorization == null) {
			return this.recipeService.getRecipeById(id, null);
		} else {
			ApplicationUser applicationUser = userService.getUserByToken(authorization);
			if (applicationUser != null) {
				return this.recipeService.getRecipeById(id, applicationUser.getId());
			} else {
				return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
						.buildResponseEntity();
			}
		}
	}

	@GetMapping(value = "/recipes/tag/{id}")
	public ResponseEntity getRecipesByTag(
			@RequestHeader(required = false, name = SecurityConstants.HEADER_STRING) String authorization,
			@PathVariable(value = "id") int id) {

		if (authorization == null) {
			return this.recipeService.getRecipesByTag(id, null);
		} else {
			ApplicationUser applicationUser = userService.getUserByToken(authorization);

			if (applicationUser != null) {
				return this.recipeService.getRecipesByTag(id, applicationUser.getId());
			} else {
				return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
						.buildResponseEntity();
			}
		}
	}

	@Secured("ROLE_USER")
	@PostMapping(value = "/recipe")
	public ResponseEntity createRecipe(@RequestHeader(SecurityConstants.HEADER_STRING) String authorization,
			@RequestBody Recipe recipe) throws TwitterException {

		ApplicationUser applicationUser = userService.getUserByToken(authorization);

		if (applicationUser != null) {
			recipe.setCreator(applicationUser);
			return this.recipeService
					.createRecipe(RecipeTransformers.toRecipeDto(recipe, applicationUser.getId()));
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
					.buildResponseEntity();
		}
	}

	@Secured("ROLE_USER")
	@PostMapping(value = "/recipe/like")
	public ResponseEntity like(@RequestHeader(SecurityConstants.HEADER_STRING) String authorization,
			@RequestBody ScoreRecipe scoreRecipe) {
		ApplicationUser applicationUser = userService.getUserByToken(authorization);

		if (applicationUser != null) {
			scoreRecipe.setApplicationUser(applicationUser);

			ScoreRecipeDto scoreRecipeDto = ScoreRecipeTransformers.toScoreRecipeDtoClear(scoreRecipe);
			ResponseEntity returnContextLiked = recipeService.isLiked(scoreRecipeDto);

			if (returnContextLiked.getStatusCode().equals(HttpStatus.OK)) {
				return this.recipeService.deleteLike(scoreRecipeDto);
			} else {
				return this.recipeService.like(ScoreRecipeTransformers.toScoreRecipeDtoClear(scoreRecipe));
			}
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
					.buildResponseEntity();
		}
	}

	@Secured("ROLE_USER")
	@PostMapping(value = "/recipe/dislike")
	public ResponseEntity dislike(@RequestHeader(SecurityConstants.HEADER_STRING) String authorization,
			@RequestBody ScoreRecipe scoreRecipe) {

		ApplicationUser applicationUser = userService.getUserByToken(authorization);
		if (applicationUser != null) {
			scoreRecipe.setApplicationUser(applicationUser);

			ScoreRecipeDto scoreRecipeDto = ScoreRecipeTransformers.toScoreRecipeDtoClear(scoreRecipe);
			ResponseEntity returnContextLiked = recipeService.isLiked(scoreRecipeDto);

			if (returnContextLiked.getStatusCode().equals(HttpStatus.OK)) {
				return this.recipeService.deleteLike(scoreRecipeDto);
			} else {
				return this.recipeService.dislike(scoreRecipeDto);
			}
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
					.buildResponseEntity();
		}
	}

	@Secured("ROLE_USER")
	@DeleteMapping(value = "/recipe/{id}")
	public ResponseEntity deleteOwnRecipe(@RequestHeader(SecurityConstants.HEADER_STRING) String authorization,
			@PathVariable(value = "id") int id) {

		ApplicationUser applicationUser = userService.getUserByToken(authorization);
		if (applicationUser != null) {
			Recipe recipe = new Recipe();
			recipe.setId(id);
			recipe.setCreator(applicationUser);
			
			ResponseEntity returnContextOwn = recipeService.isOwnRecipe(recipe.getId(), recipe.getCreator().getId());

			if (returnContextOwn.getStatusCode().equals(HttpStatus.OK)) {
				return this.recipeService.deleteRecipeById(id);
			} else {
				return new ReturnContext(HttpStatus.UNAUTHORIZED, null, "User does not have the right to delete this recipe")
						.buildResponseEntity();
			}
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "User with this token does not exist")
					.buildResponseEntity();
		}
	}
}
