package fr.lesptitsgourmets.lesptitsgourmets.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.UnitRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UnitDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Unit;
import fr.lesptitsgourmets.lesptitsgourmets.service.IUnitService;

@Service(value = "unitService")
public class UnitServiceImpl implements IUnitService {

	@Resource
	private UnitRepository unitRepository;

	@Override
	public ResponseEntity getAllUnits() {

		List<Unit> unitList = this.unitRepository.findAll();
		
		if (unitList == null) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "Error in the request.").buildResponseEntity();
		} else {
			if (unitList.isEmpty()) {
				return new ReturnContext(HttpStatus.NO_CONTENT, unitList, null).buildResponseEntity();
			} else {
				return new ReturnContext(HttpStatus.OK, unitList, null).buildResponseEntity();
			}
		}
	}

	public static UnitDto fromUnitEntity(Unit entity) {

		UnitDto dto = new UnitDto();

		dto.setId(entity.getId());
		dto.setName(entity.getName());
		
		return dto;
	}

	public static Unit toUnitEntity(UnitDto unitDto) {

		Unit entity = new Unit();

		entity.setId(unitDto.getId());
		entity.setName(unitDto.getName());

		return entity;
	}
}
