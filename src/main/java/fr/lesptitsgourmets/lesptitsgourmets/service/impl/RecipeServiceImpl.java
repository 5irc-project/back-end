package fr.lesptitsgourmets.lesptitsgourmets.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.persistence.EntityExistsException;
import javax.persistence.TransactionRequiredException;

import fr.lesptitsgourmets.lesptitsgourmets.service.twitter.TwitterService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.IngredientRecipeRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.RecipeRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.ScoreRecipeRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.TagRecipeRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.UserRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.RecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.ScoreRecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UserDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.IngredientRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Recipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ScoreRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.TagRecipe;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.IngredientRecipeId;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.ScoreRecipeId;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.compositeKey.TagRecipeId;
import fr.lesptitsgourmets.lesptitsgourmets.model.enumeration.Counter;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.RecipeTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.ScoreRecipeTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.service.IRecipeService;
import twitter4j.TwitterException;

@Service(value = "recipeService")
public class RecipeServiceImpl implements IRecipeService {

	@Resource
	private RecipeRepository recipeRepository;

	@Resource
	private TagRecipeRepository tagRecipeRepository;

	@Resource
	private IngredientRecipeRepository ingredientRecipeRepository;

	@Resource
	private ScoreRecipeRepository scoreRecipeRepository;

	@Resource
	private UserRepository userRepository;

	@Override
	public ResponseEntity getAllRecipes(String name, Integer idUser) {

		Page<Recipe> recipeList = this.recipeRepository.findAllByNameContainingIgnoreCase(name,
				PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "score")));

		if (recipeList == null) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "Error in the request.").buildResponseEntity();
		} else {
			if (recipeList.isEmpty()) {
				return new ReturnContext(HttpStatus.NO_CONTENT,
						RecipeTransformers.toRecipeDtoListClear(recipeList.getContent(), idUser), null)
								.buildResponseEntity();
			} else {
				return new ReturnContext(HttpStatus.OK,
						RecipeTransformers.toRecipeDtoListClear(recipeList.getContent(), idUser), null)
								.buildResponseEntity();
			}
		}
	}

	@Override
	public ResponseEntity deleteRecipeById(int id) {

		try {
			this.recipeRepository.deleteById(id);
			return new ReturnContext(HttpStatus.OK, null, null).buildResponseEntity();
		} catch (IllegalArgumentException error) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "No recipe found with id : " + id + ".")
					.buildResponseEntity();
		}
	}

	public ResponseEntity isLiked(ScoreRecipeDto scoreRecipeDto) {
		ScoreRecipe scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(scoreRecipeDto);

		Optional<ScoreRecipe> scoreRecipeBdd = this.scoreRecipeRepository.findById(scoreRecipe.getId());
		List<ScoreRecipeDto> scoreRecipeList = new ArrayList<ScoreRecipeDto>();

		if (scoreRecipeBdd.isPresent()) {
			scoreRecipeList.add(ScoreRecipeTransformers.toScoreRecipeDtoClear(scoreRecipeBdd.get()));
			return new ReturnContext(HttpStatus.OK, scoreRecipeList, null).buildResponseEntity();
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "No data for this id").buildResponseEntity();
		}
	}

	@Override
	public ResponseEntity like(ScoreRecipeDto scoreRecipeDto) {

		ResponseEntity returnContextLiked = isLiked(scoreRecipeDto);
		ScoreRecipe scoreRecipe;

		if (returnContextLiked.getStatusCode().equals(HttpStatus.OK)) {
			scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(
					(ScoreRecipeDto) ((ReturnContext) returnContextLiked.getBody()).getData().get(0));
		} else {
			scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(scoreRecipeDto);
			scoreRecipe.setId(
					new ScoreRecipeId(scoreRecipe.getRecipe().getId(), scoreRecipe.getApplicationUser().getId()));
		}
		scoreRecipe.setValue(Counter.PLUSONE.getName());
		this.scoreRecipeRepository.save(scoreRecipe);

		List<ScoreRecipeDto> scoreRecipeList = new ArrayList<ScoreRecipeDto>();
		scoreRecipeList.add(ScoreRecipeTransformers.toScoreRecipeDto(scoreRecipe));
		return new ReturnContext(HttpStatus.OK, scoreRecipeList, null).buildResponseEntity();
	}

	@Override
	public ResponseEntity deleteLike(ScoreRecipeDto scoreRecipeDto) {

		ResponseEntity returnContext;
		if (scoreRecipeDto.getId() != null) {

			ScoreRecipe scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(scoreRecipeDto);
			this.scoreRecipeRepository.delete(scoreRecipe);
			returnContext = new ReturnContext(HttpStatus.OK, null, "Data deleted").buildResponseEntity();
		} else {
			returnContext = new ReturnContext(HttpStatus.BAD_REQUEST, null, "ScoreRecipe id  missing")
					.buildResponseEntity();
		}
		return returnContext;
	}

	@Override
	public ResponseEntity dislike(ScoreRecipeDto scoreRecipeDto) {

		ResponseEntity returnContextLiked = isLiked(scoreRecipeDto);
		ScoreRecipe scoreRecipe;

		if (returnContextLiked.getStatusCode().equals(HttpStatus.OK)) {
			scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(
					(ScoreRecipeDto) ((ReturnContext) returnContextLiked.getBody()).getData().get(0));
		} else {
			scoreRecipe = ScoreRecipeTransformers.toScoreRecipeEntity(scoreRecipeDto);
			scoreRecipe.setId(
					new ScoreRecipeId(scoreRecipe.getRecipe().getId(), scoreRecipe.getApplicationUser().getId()));
		}
		scoreRecipe.setValue(Counter.MINUSONE.getName());
		this.scoreRecipeRepository.save(scoreRecipe);

		List<ScoreRecipeDto> scoreRecipeList = new ArrayList<ScoreRecipeDto>();
		scoreRecipeList.add(ScoreRecipeTransformers.toScoreRecipeDto(scoreRecipe));
		return new ReturnContext(HttpStatus.OK, scoreRecipeList, null).buildResponseEntity();
	}

	public ResponseEntity getRecipesByTag(int id, Integer idUser) {

		Page<Recipe> recipeList = this.recipeRepository.findByTagRecipes_Tag_Id(id,
				PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "score")));

		if (recipeList == null) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "Error in the request.").buildResponseEntity();
		} else {
			if (recipeList.isEmpty()) {
				return new ReturnContext(HttpStatus.NO_CONTENT,
						RecipeTransformers.toRecipeDtoListClear(recipeList.getContent(), idUser), null)
								.buildResponseEntity();
			} else {
				return new ReturnContext(HttpStatus.OK,
						RecipeTransformers.toRecipeDtoListClear(recipeList.getContent(), idUser), null)
								.buildResponseEntity();
			}
		}
	}

	@Override
	public ResponseEntity getRecipeById(int id, Integer idUser) {

		Optional<Recipe> recipe = this.recipeRepository.findById(id);

		if (recipe == null) {
			return new ReturnContext(HttpStatus.NO_CONTENT, null, "No recipe found with id : " + id + ".")
					.buildResponseEntity();
		} else {
			List<RecipeDto> recipeList = new ArrayList<RecipeDto>();
			recipeList.add(RecipeTransformers.toRecipeDto(recipe.get(), idUser));
			return new ReturnContext(HttpStatus.OK, recipeList, null).buildResponseEntity();
		}
	}

	@Override
	public ResponseEntity createRecipe(RecipeDto recipeDto) throws TwitterException {
		try {

			Recipe recipe = this.recipeRepository.save(RecipeTransformers.toRecipeEntity(recipeDto));

			/** SAVE TAGS **/
			for (TagRecipe tagRecipe : recipe.getTagRecipes()) {
				tagRecipe.setRecipe(recipe);
				tagRecipe.setId(new TagRecipeId(tagRecipe.getRecipe().getId(), tagRecipe.getTag().getId()));
				this.tagRecipeRepository.save(tagRecipe);
			}

			/** SAVE INGREDIENTS RECIPE **/
			for (IngredientRecipe ingredientRecipe : recipe.getIngredientrecipe()) {
				ingredientRecipe.setRecipe(recipe);
				ingredientRecipe.setId(new IngredientRecipeId(ingredientRecipe.getRecipe().getId(),
						ingredientRecipe.getIngredient().getId()));
				this.ingredientRecipeRepository.save(ingredientRecipe);
			}

			List<RecipeDto> recipeList = new ArrayList<RecipeDto>();
			recipeList.add(RecipeTransformers.toRecipeDto(recipe, null));
			TwitterService.createTweet(recipe.getName());
			return new ReturnContext(HttpStatus.OK, recipeList, null).buildResponseEntity();
		} catch (TransactionRequiredException error) {
			return new ReturnContext(HttpStatus.INTERNAL_SERVER_ERROR, null,
					"There is no active transaction for persistence").buildResponseEntity();
		} catch (IllegalArgumentException error) {
			return new ReturnContext(HttpStatus.BAD_REQUEST, null, "JSON parse error: missing data.")
					.buildResponseEntity();
		} catch (EntityExistsException error) {
			return new ReturnContext(HttpStatus.INTERNAL_SERVER_ERROR, null, "Recipe already exists.")
					.buildResponseEntity();
		}
	}

	public ResponseEntity isOwnRecipe(int idRecipe, int idCreator) {

		Optional<Recipe> recipeBdd = this.recipeRepository.findByIdAndCreatorId(idRecipe, idCreator);
		List<RecipeDto> recipeList = new ArrayList<RecipeDto>();

		if (recipeBdd.isPresent()) {
			recipeList.add(RecipeTransformers.toRecipeDto(recipeBdd.get(), recipeBdd.get().getCreator().getId()));
			return new ReturnContext(HttpStatus.OK, recipeList, null).buildResponseEntity();
		} else {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "Bad user id for this id recipe")
					.buildResponseEntity();
		}
	}
}