package fr.lesptitsgourmets.lesptitsgourmets.service.impl;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.IngredientRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Ingredient;
import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.IngredientTransformers;
import fr.lesptitsgourmets.lesptitsgourmets.service.IIngredientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityExistsException;
import javax.persistence.TransactionRequiredException;
import java.util.ArrayList;
import java.util.List;

@Service(value = "ingredientService")
public class IngredientServiceImpl implements IIngredientService {

	@Resource
	private IngredientRepository ingredientRepository;

	@Override
	public ResponseEntity getAllIngredients() {

		List<Ingredient> ingredientList = this.ingredientRepository.findAll();
		if (ingredientList == null) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "Error in the request.").buildResponseEntity();
		} else {
			if (ingredientList.isEmpty()) {
				return new ReturnContext(HttpStatus.NO_CONTENT, ingredientList, null).buildResponseEntity();
			} else {
				return new ReturnContext(HttpStatus.OK, IngredientTransformers.toIngredientDtoList(ingredientList), null).buildResponseEntity();
			}
		}
	}

	@Override
	public ResponseEntity createIngredient(IngredientDto ingredientDto) {

		try {
			Ingredient ingredient = this.ingredientRepository.save(IngredientTransformers.toIngredientEntity(ingredientDto));
			List<IngredientDto> ingredientList = new ArrayList<IngredientDto>();
			ingredientList.add(IngredientTransformers.toIngredientDto(ingredient));
			return new ReturnContext(HttpStatus.OK, ingredientList, null).buildResponseEntity();
		} catch (TransactionRequiredException error) {
			return new ReturnContext(HttpStatus.INTERNAL_SERVER_ERROR, null,
					"There is no active transaction for persistence").buildResponseEntity();
		} catch (IllegalArgumentException error) {
			return new ReturnContext(HttpStatus.BAD_REQUEST, null, "JSON parse error: missing data.").buildResponseEntity();
		} catch (EntityExistsException error) {
			return new ReturnContext(HttpStatus.INTERNAL_SERVER_ERROR, null, "Ingredient already exists.").buildResponseEntity();
		}
	}

	@Override
	public ResponseEntity deleteIngredientById(int id) {
		try {
			this.ingredientRepository.deleteById(id);
			return new ReturnContext(HttpStatus.OK, null, null).buildResponseEntity();
		} catch (IllegalArgumentException error) {
			return new ReturnContext(HttpStatus.NOT_FOUND, null, "No ingredient found with id : " + id + ".").buildResponseEntity();
		}
	}
}