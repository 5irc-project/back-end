package fr.lesptitsgourmets.lesptitsgourmets.service.impl;

import java.util.List;

import fr.lesptitsgourmets.lesptitsgourmets.model.transformers.TagTransformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import fr.lesptitsgourmets.lesptitsgourmets.model.ReturnContext.ReturnContext;
import fr.lesptitsgourmets.lesptitsgourmets.model.dao.TagRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.Tag;
import fr.lesptitsgourmets.lesptitsgourmets.service.ITagService;

@Service(value = "tagService")
public class TagServiceImpl implements ITagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public ResponseEntity getAllTags() {

        List<Tag> tagList = this.tagRepository.findAll();
        if (tagList == null) {
            return new ReturnContext(HttpStatus.NOT_FOUND, null, "Error in the request.").buildResponseEntity();
        } else {
            if (tagList.isEmpty()) {
                return new ReturnContext(HttpStatus.NO_CONTENT, tagList, null).buildResponseEntity();
            } else {
                return new ReturnContext(HttpStatus.OK, TagTransformers.toTagDtoList(tagList), null).buildResponseEntity();
            }
        }
    }
}
