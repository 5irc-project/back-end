package fr.lesptitsgourmets.lesptitsgourmets.service.impl;

import fr.lesptitsgourmets.lesptitsgourmets.model.dao.UserRepository;
import fr.lesptitsgourmets.lesptitsgourmets.model.entity.ApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		Optional<ApplicationUser> applicationUser = userRepository.findByEmail(username);
		if (applicationUser.isPresent()) {
			return new org.springframework.security.core.userdetails.User(applicationUser.get().getUsername(),
					applicationUser.get().getPassword(), applicationUser.get().getAuthorities());
		} else {
			return null;
		}
	}

	public ApplicationUser getUserByEmail(String email) {
		Optional<ApplicationUser> user = userRepository.findByEmail(email);
		if (user.isPresent()) {
			return user.get();
		} else {
			return null;
		}
	}

	public ApplicationUser updateUser(ApplicationUser user) {
		return userRepository.save(user);
	}

	public ApplicationUser getUserByToken(String token) {

		token = token.replace("Bearer ", "");
		Optional<ApplicationUser> optionalUser = userRepository.findByToken(token);
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			return null;
		}
	}
}
