package fr.lesptitsgourmets.lesptitsgourmets.service;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.IngredientDto;
import org.springframework.http.ResponseEntity;

public interface IIngredientService {

    ResponseEntity getAllIngredients();

    ResponseEntity createIngredient(IngredientDto ingredientDto);

    ResponseEntity deleteIngredientById(int id);
}
