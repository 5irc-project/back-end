package fr.lesptitsgourmets.lesptitsgourmets.service;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.RecipeDto;
import fr.lesptitsgourmets.lesptitsgourmets.model.dto.ScoreRecipeDto;

import org.springframework.http.ResponseEntity;
import twitter4j.TwitterException;

import fr.lesptitsgourmets.lesptitsgourmets.model.dto.UserDto;

public interface IRecipeService {

	ResponseEntity getAllRecipes(String name, Integer idUser);

	ResponseEntity getRecipeById(int id, Integer idUser);

    ResponseEntity createRecipe(RecipeDto recipeDto) throws TwitterException;

	ResponseEntity deleteRecipeById(int id);

	ResponseEntity like(ScoreRecipeDto scoreRecipeDto);

	ResponseEntity dislike(ScoreRecipeDto scoreRecipeDto);

	ResponseEntity getRecipesByTag(int id, Integer idUser);

	ResponseEntity deleteLike(ScoreRecipeDto scoreRecipeDto);
}