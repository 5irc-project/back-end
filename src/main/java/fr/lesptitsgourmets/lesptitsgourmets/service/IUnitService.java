package fr.lesptitsgourmets.lesptitsgourmets.service;

import org.springframework.http.ResponseEntity;

public interface IUnitService {

    ResponseEntity getAllUnits();
}
