package fr.lesptitsgourmets.lesptitsgourmets.service;

import org.springframework.http.ResponseEntity;

public interface ITagService {

    ResponseEntity getAllTags();
}
