package fr.lesptitsgourmets.lesptitsgourmets.service.twitter;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterService {

    private static final String FRONT_URL_APP = "https://lesptitsgourmets.herokuapp.com/";

    public static Twitter getTwitterinstance() {

        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }

    public static String createTweet(String tweet) throws TwitterException {
        Twitter twitter = getTwitterinstance();
        Status status = twitter.updateStatus("La nouvelle recette : "+ tweet + "\nvient d'être créée ! Viens vite la voir sur " + FRONT_URL_APP);
        return status.getText();
    }
}
